There are various ways in which you can contribute to this project:

- review open issues, see tags #goodfirstissue if you are new to this project

List of current contributors below:

- Ewan Findley
- Luis Tiago
- Naomi Joy Smith
- Aimee Fenech
