Convergence web infrastructure
Last reviewed: 10 January 2021, 10 January 2020 (26 June 2020)
Reviewed by: (Aimee Fenech previously Nenad Maljković, Philipp Grunewald)
Consented into usage by Capacity+ team on 9 June 2020 

How to submit a proposal? 
YOU CAN DELETE THIS BOX WHEN YOUR PROPOSAL IS COMPLETE 

A member of the core team can help you with your proposal - contact us in the #capacityplusfunding channel

This is a template; please use it as a basis to create your own proposals. After creating a copy of this document (without copying possible comments) change title above into title of your proposal. Also change the document name in the upper left corner. Please write the content of your proposal between question boxes below. 

If you change the structure of the document (to better suit the particularities of your proposal) please make sure that you still answer the questions that we expect to be covered by any proposal we review as a team. 

Once you completed the first draft of your proposal please submit it preferably to #capacityplusfunding channel on Slack as message or elsewhere on Slack where appropriate (in some other Slack channel as a message or in a thread) and tag @cpluscore user group. 

    • Proposal prepared by: Aimee Fenech, Naomi Smith, Ewan Findley and Luis Tiago
    • Proposal date: [add date here] 
    • Decision-making thread: [add link here] 
    • Capacity+ Project Core Team (@cpluscore user group on Slack) consent required within 14 days of proposal date above. If no objection raised by then proposal is considered consented. 
    • Outcome: [for example: consented on [date]] 

1. Description of the proposal
Note: as short as possible, max. 150 words, for a public audience (no jargon, technical language, etc.) 
PAB has approached the Digital circle for assistance with the upcoming EUPC 2021 to be held in October. The EUPC team has access to some funding which will cover the following:
* consultation time with digital circle,
* the eupc domain costs for how long?
* hosting costs for how long?
*

2. Why should this be funded through Capacity+?
Note: The rationale for why this should be approved. How does it align with Capacity+ domain and aims? How does it improve upon and fit in with the work Capacity+ is currently doing? How does it increase the capacity of the CoLab to fulfil its own (the CoLab’s) aims? Explicit references to the Capacity+ proposal makes this much stronger! 
Oneco capacity+ deliverables directly addresses support for the IPCC international event however as this was not possible the team has come up with a resource that can be developed not only for use of IPCC but also any other permaculture or aligned convergence and offer this as a commons.

3. What are you committing to deliver?
Note: What are the tangible things you are committing to delivering? What is it we and everyone else will see when you are done? Focus on deliverables, outputs, products, etc… keep it tangible and practical. A list or table might work for most projects. 
We want to create a convergence website that can be used for IPCC / PiNZ and EUPC, taking EUPC as a working example.  Including the following functions:
landing page, event management

This will be offered on a opensource basis ie free to copy of use for the commons, together with an offer of additional optional services as cost (eg tech support, hosting, domain management etc.)

4. What value are you going to create?
Note: who is going to benefit in what way from the above deliverables? Also, if there are intangible outcomes from your work please outline those here. 
Permaculture and allied organisations will benefit from the resources created for the commons. By making this opensource we also ensure that there is a possibility to improve on this work in future.

5. How are you going to go about it?
Note: what is the plan or process? Who is involved? What are the dependencies? How much input (hours, resources, etc.) are you anticipating will be required? 
We have had some exploratory meetings within the team and also with the EUPC team, the plan is for the tech part of the team to go ahead an build the MVP for EUPC first and use that as a springboard to get the convergence web template started.

The MVP for EUPC will be on by the end of July, with additional features to be added on gradually at the teams work on them.

Naomi and Aimee will be surveying some convergence organisers to help inform this work and keep it relevant.

Luis and Ewan will be concentrating their work on the implementation side of the project.

6. What resources are you requesting?
Note: this could be money, this could be input and support from (other) core members, use of tools available in our ecosystem. Specific references to the Capacity+ proposal, budget, and budget narrative make this much stronger! Check C+ FINANCIAL working doc for available funds in budget line items. When requesting money please use currency £ GBP. 
Below are the estimate number of hours and the rates applicable for each team member’s contribution to this project.
Luis
Ewan
Naomi
Aimee

7. How do you suggest we hold you accountable? 
Note: this is an opportunity for you to suggest appropriate ways of being accountable for your work. Who is supposed to hold you accountable in what ways? Who needs to let the C+ team know that your proposal has met expectations? If you are delivering value to a group other than C+, how will they let us know that they are satisfied? How is the Capacity+ Core Team going to implement whatever you suggest? 
Weekly updates in the c+ team, once the project is completed a survey report will be published together with a web page describing this resources, how to download it and detailing supporting services available in connection with this.

Use space below to capture comments, answers to clarifying questions, etc. only if for some reason you need to move them here from a decision-making thread in Slack. 


-- END OF DOCUMENT -- 
